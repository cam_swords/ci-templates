#!/usr/bin/env bash

set -e

export IMAGE_TAG="$TMP_IMAGE$IMAGE_TAG_SUFFIX"

if [[ -n "$BUILD_MULTI_ARCHITECTURE_IMAGE" ]]; then
  echo "Building multi-architecture image"

  # Make sure CI has appropriate platform information installed
  docker run --privileged --rm tonistiigi/binfmt --uninstall arm64
  docker run --privileged --rm tonistiigi/binfmt --install arm64

  export CACHE_IMAGE="$CI_REGISTRY_IMAGE/build-cache/build-$CI_COMMIT_REF_SLUG:cache$IMAGE_TAG_SUFFIX"
  docker buildx create --name analyzer-builder
  docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
  docker buildx build \
    --build-arg GO_VERSION \
    --platform=linux/arm64,linux/amd64 \
    --tag ${IMAGE_TAG} \
    --cache-to=type=registry,ref=${CACHE_IMAGE} \
    --cache-from=type=registry,ref=${CACHE_IMAGE} \
    --file ${DOCKERFILE} \
    --builder=analyzer-builder \
    --push \
    --provenance=false \
    .
else
  echo "Building image"

  docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
  docker build --build-arg GO_VERSION -t "$IMAGE_TAG" -f "$DOCKERFILE" .
  docker push "$IMAGE_TAG"
fi

echo "Built docker image $IMAGE_TAG"
