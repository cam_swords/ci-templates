#!/usr/bin/env bash

set -e

SCRIPTS_DIR="$(realpath "$(cd "$(dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd)")"

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$SCRIPTS_DIR/docker_utils.sh"

SOURCE_IMAGE="$TMP_IMAGE$IMAGE_TAG_SUFFIX"

if [[ -n "$BUILD_MULTI_ARCHITECTURE_IMAGE" ]]; then
  echo "Tagging multi-architecture image"

  TARGET_IMAGE="$IMAGE_NAME:$IMAGE_TAG$IMAGE_TAG_SUFFIX"
  tag_and_push_image "$SOURCE_IMAGE" "$TARGET_IMAGE" "$CI_REGISTRY" "gitlab-ci-token" "$CI_JOB_TOKEN"

  if [[ -n "$SEC_REGISTRY_IMAGE" && -n "$SEC_REGISTRY_PASSWORD" && -n "$SEC_REGISTRY_USER" ]]; then
    SEC_REGISTRY="${SEC_REGISTRY_IMAGE%%/*}"
    TARGET_IMAGE="$SEC_REGISTRY_IMAGE:$IMAGE_TAG$IMAGE_TAG_SUFFIX"
    tag_and_push_image "$SOURCE_IMAGE" "$TARGET_IMAGE" "$CI_REGISTRY" "gitlab-ci-token" "$CI_JOB_TOKEN" "$SEC_REGISTRY" "$SEC_REGISTRY_USER" "$SEC_REGISTRY_PASSWORD"
  fi
else
  # single architecture image tagging
  echo "Tagging image"

  TARGET_IMAGE="$IMAGE_NAME:$IMAGE_TAG$IMAGE_TAG_SUFFIX"
  docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
  docker pull "$SOURCE_IMAGE"
  docker tag "$SOURCE_IMAGE" "$TARGET_IMAGE"
  docker push "$TARGET_IMAGE"

  if [[ -n "$SEC_REGISTRY_IMAGE" && -n "$SEC_REGISTRY_PASSWORD" && -n "$SEC_REGISTRY_USER" ]]; then
    SEC_REGISTRY="${SEC_REGISTRY_IMAGE%%/*}"
    docker login -u "$SEC_REGISTRY_USER" -p "$SEC_REGISTRY_PASSWORD" "$SEC_REGISTRY"
    TARGET_IMAGE="$SEC_REGISTRY_IMAGE:$IMAGE_TAG$IMAGE_TAG_SUFFIX"
    docker tag "$SOURCE_IMAGE" "$TARGET_IMAGE"
    docker push "$TARGET_IMAGE"
  fi
fi
