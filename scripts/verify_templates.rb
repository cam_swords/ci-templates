#!/usr/bin/env ruby

require 'net/http'
require 'json'

CI_TEMPLATES_PROJECT_ID = 55820257
LINTER_URI = URI.parse "https://gitlab.com/api/v4/projects/#{CI_TEMPLATES_PROJECT_ID}/ci/lint"
STUB = <<YAML
  include:
    - https://gitlab.com/gitlab-org/cam_swords/ci-templates/raw/#{ENV.fetch('CI_COMMIT_REF_NAME', 'master')}/includes-dev/stub.yml
YAML

def verify(file)
  content = STUB + IO.read(file)
  req = Net::HTTP::Post.new(LINTER_URI)
  req.form_data = {content: content}
  req.add_field("PRIVATE-TOKEN", ENV["GITLAB_TOKEN"])
  response = Net::HTTP.start(LINTER_URI.hostname, LINTER_URI.port, use_ssl: true) { |http| http.request(req) }

  file = File.basename(file)

  json = JSON.parse(response.body)
  if json['valid'] == true
    puts "\e[32mvalid\e[0m: #{file}" # Color 'valid' green
  else
    puts "invalid: #{file}"
    puts "%s %s" % [response.code, response.message]
    puts json['errors']
    exit(1)
  end
end

if ARGV.empty?
  Dir.glob("#{__dir__}/../**/*.yml").each { |file| verify(file) }

  # Given we test all the templates, the coverage is 100%, always. To showcase
  # how this is done, we print it here.
  # Regexp used to parse this: Coverage:\s(\d+)
  puts 'Coverage: 100%'
else
  verify(ARGV[0])
end
