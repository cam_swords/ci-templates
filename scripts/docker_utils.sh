
# error prints an error message and exits. builds a payload that can be used to create a GitLab release via the API.
# arguments: [Message...]
error() {
  printf "\n"
  printf "%s" "$*" >>/dev/stderr
  printf "\n"
  exit 1
}

# verify_has_value errors with given message if the variable is not set.
# arguments: [Variable] [Message]
verify_has_value() {
  variable="$1"
  message="$2"

  if [[ -z $variable ]]; then
    error "$message"
  fi
}

# debug_log prints a message.
# arguments: [Message...]
debug_log() {
  printf "\n\n# %s\n" "$*" >>/dev/stdout
}

# login_to_registry logs in to a remote registry
# arguments: [Registry Username] [Registry Password] [CI Registry]
login_to_registry() {
  local registry_username="$1"
  local registry_password="$2"
  local ci_registry="$3"

  verify_has_value "$registry_username" "Aborting, ci registry username has not been supplied to ${FUNCNAME[0]}"
  verify_has_value "$registry_password" "Aborting, ci registry password has not been supplied to ${FUNCNAME[0]}"
  verify_has_value "$ci_registry" "Aborting, ci registry has not been supplied to ${FUNCNAME[0]}"

  debug_log "logging into $ci_registry"
  docker login -u "$registry_username" -p "$registry_password" "$ci_registry"
}

# pulls an image from the registry logs in to a remote registry
# arguments: [Image]
pull_from_registry() {
  local image_name="$1"

  verify_has_value "$image_name" "Aborting, image has not been supplied to ${FUNCNAME[0]}"

  if ! docker image inspect "$image_name" >/dev/null 2>/dev/null; then
    debug_log "pulling image from registry $image_name"
    docker pull "$image_name"
  fi
}

# tag_and_push_image tags a docker image
# arguments: [from] [from_registry] [from_username] [from_password] [to] [to_registry] [to_username] [to_password]
# This will log into the from registry and the to registry if credentials are supplied
tag_and_push_image() {
  local from="$1"
  local to="$2"

  # to registry credentials are optional, only required when pushing to an external registry
  local from_registry="$3"
  local from_username="$4"
  local from_password="$5"
  local to_registry="$6"
  local to_username="$7"
  local to_password="$8"

  verify_has_value "$from" "Aborting, unable to tag image as the from location has not been supplied to ${FUNCNAME[0]}"
  verify_has_value "$to" "Aborting, unable to tag image as the to location has not been supplied to ${FUNCNAME[0]}"

  debug_log "tagging from $from to $to"

  if [[ "$from_username" != "" ]] && [[ "$from_password" != "" ]] && [[ "$from_registry" != "" ]]; then
    debug_log "from credentials supplied, authenticating to source registry"
    login_to_registry "$from_username" "$from_password" "$from_registry"
  fi

  pull_from_registry "$from"

  debug_log "retrieving manifest of from image"
  from_manifest=$(docker manifest inspect "$from")

  if [[ "$to_username" != "" ]] && [[ "$to_password" != "" ]] && [[ "$to_registry" != "" ]]; then
    debug_log "to credentials supplied, authenticating to destination registry"
    login_to_registry "$to_username" "$to_password" "$to_registry"
  fi

  create_manifest_cmd="docker manifest create $to"
  from_image_without_tag=${from%:*}
  to_image_without_tag=${to%:*}
  num_manifests=$(echo "$from_manifest" | jq '.manifests | length')

  if [ "$num_manifests" == "0" ]; then
    error "Failed to find any manifests to tag in $from_manifest"
  fi

  debug_log "Found $num_manifests manifests in $from"

  for i in $(seq 1 "$num_manifests"); do
    architecture=$(echo "$from_manifest" | jq ".manifests[$i - 1].platform.architecture" --raw-output)
    digest=$(echo "$from_manifest" | jq ".manifests[$i - 1].digest" --raw-output)

    # push a temporary image to the destination registry to avoid the GitLab "manifest blob unknown: blob unknown to registry" error
    temp_to="$to-$architecture"
    debug_log "Pushing temporary image to destination registry to avoid blob unknown error $temp_to"

    docker image pull "${from_image_without_tag}@${digest}"
    docker tag "${from_image_without_tag}@${digest}" "$temp_to"
    docker push "$temp_to"
    temp_to_manifest=$(docker manifest inspect "$temp_to" --verbose)
    temp_to_digest=$(echo "$temp_to_manifest" | jq ".Descriptor.digest" --raw-output)

    create_manifest_cmd="${create_manifest_cmd} ${to_image_without_tag}@${temp_to_digest}"
  done

  debug_log "Creating final multi-architecture image $to"
  echo "$create_manifest_cmd"
  eval "$create_manifest_cmd"
  docker manifest push "$to"
}
